package com.seis770;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/24/13
 * Time: 10:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class StockBroker implements Observer {

    StockBroker uniqueInstance;
    List<Stock> stocks = new ArrayList<Stock>();

    protected StockBroker() {

    }

    public StockBroker getUniqueInstance() {
        if(this.uniqueInstance == null) {
            this.uniqueInstance = new StockBroker();
        }
        return this.uniqueInstance;
    }


    @Override
    public void update(Observable observable, Object o) {
        this.setStock((Stock)observable);
        System.out.println("I have access to these stocks:");
        for(Stock stock : this.stocks) {
            System.out.println("Stock symbol:" + stock.getStockSymbol());
        }

    }

    public StockStatus getCurrentStockStatus(String stockSymbol) {
        for(Stock stock : this.stocks) {
            if(stock.getStockSymbol().equals(stockSymbol)) {
                return stock.getCurrentStockStatus();
            }
        }
        return null;
    }

    public void setStock(Stock stock) {
        this.stocks.add(stock);
    }

    public Stock getStock(String stockSymbol) {
        for(Stock stock : this.stocks) {
            if(stock.getStockSymbol().equals(stockSymbol)) {
                return stock;
            }
        }
        return null;
    }
}
