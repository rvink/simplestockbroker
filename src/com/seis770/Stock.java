package com.seis770;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/24/13
 * Time: 9:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class Stock extends Observable {

    String stockSymbol;
    List<StockStatus> stockStatuses = new ArrayList<StockStatus>();

    public Stock(String stockSymbol,StockStatus stockStatus,StockBroker stockBroker) {
        this.stockSymbol = stockSymbol;
        this.stockStatuses.add(stockStatus);
        this.addObserver(stockBroker);
        this.setChanged();
        this.notifyObservers();
    }

    public static Stock createStock(String stockSymbol,StockStatus stockStatus,StockBroker stockBroker) {
        return new Stock(stockSymbol,stockStatus,stockBroker);
    }

    public String getStockSymbol() {
        return this.stockSymbol;
    }

    public void addStatus(StockStatus status) {
        this.stockStatuses.add(status);
    }

    public StockStatus getCurrentStockStatus() {
        return this.stockStatuses.get(this.stockStatuses.size() - 1);
    }

    public List<StockStatus> getStockStatuses() {
        List<StockStatus> stockStatusArrayList = this.stockStatuses;
        return stockStatusArrayList;
    }
}
