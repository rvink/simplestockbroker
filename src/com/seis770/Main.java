package com.seis770;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
	    StockBroker stockBroker = new StockBroker();
        /* create the stock and get and give it an observable */
        Stock stock1 = Stock.createStock("abc", new StockStatus(new Date(), new Money()),stockBroker.getUniqueInstance());
        Stock stock2 = Stock.createStock("def",new StockStatus(new Date(),new Money()),stockBroker.getUniqueInstance());

    }

    public void doSomethingWithStockStatus(String stockSymbol) {

    }
}
