package com.seis770;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ryanvink
 * Date: 10/24/13
 * Time: 9:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class StockStatus {

    Date date;
    Money price;

    public StockStatus(Date date,Money price) {
        this.date = date;
        this.price = price;
    }

    public Money getPrice() {
        return this.price;
    }

    public Date getDate() {
        return this.date;
    }

}
